module.exports = {
  title: "Ultainfinity Coin Program Library Docs",
  tagline:
    "Ultainfinity Coin is an open source project implementing a new, high-performance, permissionless blockchain.",
  url: "https://spl.uttacoin.com",
  baseUrl: "/",
  favicon: "img/favicon.ico",
  organizationName: "solana-labs", // Usually your GitHub org/user name.
  projectName: "solana-program-library", // Usually your repo name.
  themeConfig: {
    navbar: {
      logo: {
        alt: "Ultainfinity Coin Logo",
        src: "img/logo-horizontal.svg",
        srcDark: "img/logo-horizontal-dark.svg",
      },
      links: [
        {
          href: "https://docs.uttacoin.com/",
          label: "Docs »",
          position: "left",
        },
        {
          href: "https://discordapp.com/invite/pquxPsq",
          label: "Chat",
          position: "right",
        },

        {
          href: "https://bitbucket.org/sumraprague/utta-program-library.git",
          label: "GitHub",
          position: "right",
        },
      ],
    },
    footer: {
      style: "dark",
      links: [
        {
          title: "Community",
          items: [
            {
              label: "Discord",
              href: "https://discordapp.com/invite/pquxPsq",
            },
            {
              label: "Twitter",
              href: "https://twitter.com/solana",
            },
            {
              label: "Forums",
              href: "https://forums.uttacoin.com",
            },
          ],
        },
        {
          title: "More",
          items: [
            {
              label: "GitHub",
              href: "https://bitbucket.org/sumraprague/utta-program-library.git",
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Ultainfinity Coin Foundation`,
    },
  },
  plugins: [require.resolve('docusaurus-lunr-search')],
  presets: [
    [
      "@docusaurus/preset-classic",
      {
        docs: {
          path: "src",
          routeBasePath: "/",
          homePageId: "introduction",
          sidebarPath: require.resolve("./sidebars.js"),
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      },
    ],
  ],
};
