# Name Service JavaScript bindings

[![npm](https://img.shields.io/npm/v/@uttacoin/spl-name-service)](https://unpkg.com/@uttacoin/spl-name-service@latest/) ![GitHub license](https://img.shields.io/badge/license-APACHE-blue.svg)

Based on Solana Program Library [https://github.com/solana-labs/solana-program-library/tree/master/name-service](https://github.com/solana-labs/solana-program-library/tree/master/name-service)

Forked 22.11.2021

Full documentation is available at https://spl.uttacoin.com/name-service

JavaScript binding allow to interact with a spl program for issuing and managing
ownership of: domain names, Ultainfinity Coin Pubkeys, URLs, twitter handles, arweave ids,
metadata, etc..

This package provides an interface that third parties can
utilize to create and use their own version of a name service of any kind.

## Installation

```bash
npm install @uttacoin/spl-name-service
```

```bash
yarn add @uttacoin/spl-name-service
```
