# SPL Token Lending client library

This is a JavaScript + TypeScript library for interacting with the [SPL Token Lending](https://bitbucket.org/sumraprague/utta-program-library.git/tree/master/token-lending) program.

## Install

Install the library and its peer dependencies in your app:

### Yarn
```shell
yarn add @uttacoin/spl-token-lending @uttacoin/spl-token @uttacoin/web3.js
```

### NPM
```shell
npm install @uttacoin/spl-token-lending @uttacoin/spl-token @uttacoin/web3.js
```

## Documentation

- [Client library docs](https://solana-labs.github.io/solana-program-library/token-lending/)
- [Program docs](https://bitbucket.org/sumraprague/utta-program-library.git/tree/master/token-lending)
- [CLI docs](https://bitbucket.org/sumraprague/utta-program-library.git/tree/master/token-lending/cli)
