# Token Lending program

A lending protocol for the Token program on the Ultainfinity Coin blockchain inspired by Aave and Compound.

Full documentation is available at https://spl.uttacoin.com/token-lending

Web3 bindings are available in the `./js` directory.

### On-chain programs

Please note that only the lending program deployed to devnet is currently operational.

| Cluster | Program Address |
| --- | --- |
| Mainnet Beta | [`LendZqTs8gn5CTSJU1jWKhKuVpjJGom45nnwPb2AMTi`](https://explorer.uttacoin.com/address/LendZqTs7gn5CTSJU1jWKhKuVpjJGom45nnwPb2AMTi) |
| Testnet | [`2xyxM9gZqk85BbdddHScsLgGdQL9sRBzGeDTfQUCcRZK`](https://explorer.uttacoin.com/address/2xyxM9gZqk85BbdddHScsLgGdQL9sRBzGeDTfQUCcRZK?cluster=testnet) |
| Devnet | [`2xyxM9gZqk85BbdddHScsLgGdQL9sRBzGeDTfQUCcRZK`](https://explorer.uttacoin.com/address/2xyxM9gZqk85BbdddHScsLgGdQL9sRBzGeDTfQUCcRZK?cluster=devnet) |

### Documentation

- [CLI docs](https://bitbucket.org/sumraprague/utta-program-library.git/tree/master/token-lending/cli)
- [Client library docs](https://solana-labs.github.io/solana-program-library/token-lending/)

### Deploy a lending program (optional)

This is optional! You can skip these steps and use the [Token Lending CLI](./cli/README.md) with one of the on-chain programs listed above to create a lending market and add reserves to it.

1. [Install the Ultainfinity Coin CLI](https://docs.uttacoin.com/cli/install-solana-cli-tools)

1. Install the Token and Token Lending CLIs:
   ```shell
   cargo install spl-token-cli
   cargo install spl-token-lending-cli
   ```
   
1. Clone the SPL repo:
   ```shell
   git clone https://bitbucket.org/sumraprague/utta-program-library.git.git
   ```

1. Go to the new directory:
   ```shell
   cd solana-program-library
   ```

1. Generate a keypair for yourself:
   ```shell
   solana-keygen new -o owner.json

   # Wrote new keypair to owner.json
   # ================================================================================
   # pubkey: JAgN4SZLNeCo9KTnr8EWt4FzEV1UDgHkcZwkVtWtfp6P
   # ================================================================================
   # Save this seed phrase and your BIP39 passphrase to recover your new keypair:
   # your seed words here never share them not even with your mom
   # ================================================================================
   ```
   This pubkey will be the owner of the lending market that can add reserves to it.

1. Generate a keypair for the program:
   ```shell
   solana-keygen new -o lending.json

   # Wrote new keypair to lending.json
   # ============================================================================
   # pubkey: 2xyxM9gZqk85BbdddHScsLgGdQL9sRBzGeDTfQUCcRZK
   # ============================================================================
   # Save this seed phrase and your BIP39 passphrase to recover your new keypair:
   # your seed words here never share them not even with your mom
   # ============================================================================
   ```
   This pubkey will be your Program ID.

1. Open `./token-lending/program/src/lib.rs` in your editor. In the line
   ```rust
   solana_program::declare_id!("2xyxM9gZqk85BbdddHScsLgGdQL9sRBzGeDTfQUCcRZK");
   ```
   replace the Program ID with yours.

1. Build the program binaries:
   ```shell
   cargo build
   cargo build-bpf
   ```

1. Prepare to deploy to devnet:
   ```shell
   solana config set --url https://api.devnet.uttacoin.com
   ```

1. Score yourself some sweet UTTA:
   ```shell
   solana airdrop -k owner.json 10
   solana airdrop -k owner.json 10
   solana airdrop -k owner.json 10
   ```
   You'll use this for transaction fees, rent for your program accounts, and initial reserve liquidity.

1. Deploy the program:
   ```shell
   solana program deploy \
     -k owner.json \
     --program-id lending.json \
     target/deploy/spl_token_lending.so

   # Program Id: 2xyxM9gZqk85BbdddHScsLgGdQL9sRBzGeDTfQUCcRZK
   ```
   If the deployment doesn't succeed, follow [this guide](https://docs.uttacoin.com/cli/deploy-a-program#resuming-a-failed-deploy) to resume it.

1. Wrap some of your UTTA as an SPL Token:
   ```shell
   spl-token wrap \
      --fee-payer owner.json \
      10.0 \
      -- owner.json

   # Wrapping 10 UTTA into AJ2sgpgj6ZeQazPPiDyTYqN9vbj58QMaZQykB9Sr6XY
   ```
   You'll use this for initial reserve liquidity. Note the SPL Token account pubkey (e.g. `AJ2sgpgj6ZeQazPPiDyTYqN9vbj58QMaZQykB9Sr6XY`).

1. Use the [Token Lending CLI](./cli/README.md) to create a lending market and add reserves to it.
