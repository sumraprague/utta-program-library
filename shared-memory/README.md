# Shared memory program

A shared-memory program on the Ultainfinity Coin blockchain, usable for sharing data
between programs or within cross-program invocations.

Full documentation is available at https://spl.uttacoin.com/shared-memory
