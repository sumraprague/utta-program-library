# Token Swap Program

A Uniswap-like exchange for the Token program on the Ultainfinity Coin blockchain, deployed
to `3ii9D3ajMqNn85B4uKmQGbh65po2jifEArSgcizV9gf1` on all networks.

Full documentation is available at https://spl.uttacoin.com/token-swap

JavaScript bindings are available in the `./js` directory.

## Building

To build a development version of the Token Swap program, you can use the normal build command for Ultainfinity Coin programs:

```sh
cargo build-bpf
```

For production versions, the Token Swap Program contains a `production` feature
to fix constraints on fees and fee account owner. A developer can
deploy the program, allow others to create pools, and earn a "protocol fee" on
all activity.

Since Ultainfinity Coin programs cannot contain any modifiable state, we must hard-code
all constraints into the program.  `SwapConstraints` in `program/src/constraints.rs`
contains all hard-coded fields for fees.  Additionally the
`SWAP_PROGRAM_OWNER_FEE_ADDRESS` environment variable specifies the public key
that must own all fee accounts.

You can build the production version of Token Swap running on devnet, testnet, and
mainnet using the following command:

```sh
SWAP_PROGRAM_OWNER_FEE_ADDRESS=GN5yicL7qznwxj3gTbedww1kMKs6pVbXd1hWZqHxSzhS cargo build-bpf --features=production
```

## Testing

### Unit tests

Run unit tests from `./program/` using:

```sh
cargo test
```

### Fuzz tests

Using the Rust version of `honggfuzz`, we "fuzz" the Token Swap program every night.
Install `honggfuzz` with:

```sh
cargo install honggfuzz
```

From there, run fuzzing from `./program/fuzz` with:

```sh
cargo hfuzz run token-swap-instructions
```

If the program crashes or errors, `honggfuzz` dumps a `.fuzz` file in the workspace,
so you can debug the failing input using:

```sh
cargo hfuzz run-debug token-swap-instructions hfuzz_workspace/token-swap-instructions/*fuzz
```

This command attaches a debugger to the test, allowing you to easily see the
exact problem.

### Integration tests

You can test the JavaScript bindings and on-chain interactions using
`solana-test-validator`, included in the Ultainfinity Coin Tool Suite.  See the
[CLI installation instructions](https://docs.uttacoin.com/cli/install-solana-cli-tools).

From `./js`, install the required modules:

```sh
npm i
```

Then run all tests:

```sh
npm run start-with-test-validator
```

If you are testing a production build, use:

```sh
SWAP_PROGRAM_OWNER_FEE_ADDRESS="GN5yicL7qznwxj3gTbedww1kMKs6pVbXd1hWZqHxSzhS" npm run start-with-test-validator
```
