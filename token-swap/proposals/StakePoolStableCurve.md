# Combine Stable Curve with Stake Pool for price discovery

Implement a curve for token-swap that accepts stake pool account and uses stable curve to swap assets. 
[Stake pool account](https://bitbucket.org/sumraprague/utta-program-library.git/blob/master/stake-pool/program/src/state.rs#L17) will be used for initial price discovery then stable curve would be used to derive price based on the size of the order and available reserves.

## Links
1. Stake pool implementation: https://bitbucket.org/sumraprague/utta-program-library.git/blob/master/stake-pool/program
2. AMM implementation: https://bitbucket.org/sumraprague/utta-program-library.git/blob/master/token-swap/program
3. Stable curve: https://bitbucket.org/sumraprague/utta-program-library.git/blob/master/token-swap/program/src/curve/stable.rs
