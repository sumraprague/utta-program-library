# Token program

A token program on the Ultainfinity Coin blockchain, usable for fungible and non-fungible tokens.

This program provides an interface and implementation that third parties can
utilize to create and use their tokens.

Full documentation is available at https://spl.uttacoin.com/token

JavaScript bindings are available in the `./js` directory.
